<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Permission::insert([
    		[
    			'name' => 'View developer dashboard',
    			'slug' => 'view-developer-dashboard',
    		],
    		[
    			'name' => 'View admin dashboard',
    			'slug' => 'view-admin-dashboard',
    		]
    	]);
    }
}
