<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Role::insert([
    		[
    			'name' => 'Developer',
    			'slug' => 'developer',
    		],
    		[
    			'name' => 'Admin',
    			'slug' => 'admin',
    		]
    	]);

        $developerRole        = Role::where('slug', 'developer')
                                ->firstOrFail();
        $developerPermissions = Permission::whereIn('slug', ['view-developer-dashboard'])
                                ->get()
                                ->pluck('id')
                                ->toArray();
        $developerRole->permissions()->sync($developerPermissions);

        $adminRole        = Role::where('slug', 'admin')
                            ->firstOrFail();
        $adminPermissions = Permission::whereIn('slug', ['view-admin-dashboard'])
                            ->get()
                            ->pluck('id')
                            ->toArray();
        $adminRole->permissions()->sync($adminPermissions);
    }
}
