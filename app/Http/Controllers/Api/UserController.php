<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\UserResource;
use App\Actions\User\UpdateUserDetailsAction;
use App\Actions\User\UpdatePasswordAction;
use App\Http\Requests\ChangeDetailsRequest;
use App\Http\Requests\ChangePasswordRequest;
use Gate;

class UserController extends Controller
{
	public function me(Request $request)
	{
		// if (Gate::allows('view-developer-dashboard')) {
			return new UserResource($request->user());
		// }
	}

	public function updateDetails(ChangeDetailsRequest $request, UpdateUserDetailsAction $updateUserDetailsAction)
	{
		if ($updateUserDetailsAction->run($request->all(), $request->user()->id)) {
			return response()->json([
				'success' => true,
			]);
		}

		return response()->json([
			'success' => false,
		]);
	}

	public function updatePassword(ChangePasswordRequest $request, UpdatePasswordAction $updatePasswordAction)
	{
		if ($updatePasswordAction->run($request->all(), $request->user()->id)) {
			return response()->json([
				'success' => true,
			]);
		}

		return response()->json([
			'success' => false,
		]);
	}
}
