<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Verified;
use App\Models\User;

class VerificationController extends Controller
{
    public function verify(Request $request)
    {
    	$user = User::find($request->id);

        if (! hash_equals((string) $request->hash, sha1($user->getEmailForVerification()))) {
            return response()->json([
				'messages' => 'Unauthorized!',
				'success'  => false
            ]);
        }

        if ($user->hasVerifiedEmail()) {
            return response()->json([
				'messages' => 'User already verify!',
				'success'  => false
            ]);
        }

        if ($user->markEmailAsVerified()) {
            event(new Verified($user));
        }

        return response()->json([
			'messages' => 'Email verified successfully!',
			'success'  => true,
        ]);
    }

    public function resend(Request $request)
    {
    	$user = User::where('email', $request->email)->first();

    	if (!$user) {
	        return response()->json([
				'messages' => 'Email not found!',
				'success'  => false,
	        ]);
    	}

        $user->sendEmailVerificationNotification();

        return response()->json([
			'messages' => 'Verification link has been sent, please check your email!',
			'success'  => true,
        ]);
    }
}
