<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use App\Models\User;
use App\Models\SocialAccount;
use App\Actions\Auth\LoginAction;
use App\Actions\Auth\RegisterAction;
use Str;

class SocialAuthController extends Controller
{
	public function redirectToProvider($provider)
	{
		$url = Socialite::driver($provider)->stateless()->redirect()->getTargetUrl();

		return response()->json([
			'url' => $url,
		]);
	}

	public function handleProviderCallback($provider, RegisterAction $registerAction)
	{
		$user = Socialite::driver($provider)->stateless()->user();

		if (!$user->token) {
			return response()->json([
				'success'  => false,
				'messages' => 'Failed to login.'
			], 401);
		}

		$checkUser = User::whereEmail($user->email)->first();

		if (!$checkUser) {
			// create user
			$checkUser = $registerAction->run([
				'name'     => $user->name,
				'email'    => $user->email,
				'password' => Str::random(6),
			]);

			$newProvider = SocialAccount::create([
				'provider'         => $provider,
				'provider_user_id' => $user->id,
				'user_id'          => $checkUser->id,
			]);
		} else {
			$checkProvider = $checkUser->socialAccounts()->where('provider', $provider)->first();

			if (!$checkProvider) {
				// add provider
				$newProvider = SocialAccount::create([
					'provider'         => $provider,
					'provider_user_id' => $user->id,
					'user_id'          => $checkUser->id,
				]);
			}
		}

		$passportToken = $checkUser->createToken('Login token');

		return response()->json([
			'access_token' => $passportToken->accessToken
		]);
	}
}
