<?php

namespace App\Actions\Auth;

use App\Models\User;
use App\Models\Role;
use Hash;

/**
 *
 */
class RegisterAction
{
	public function run($request)
	{
		$devRole = Role::where('slug', 'developer')->first();

		$user = User::create([
			'name'     => $request['name'],
			'email'    => $request['email'],
			'password' => Hash::make($request['password']),
		]);

		$user->roles()->attach($devRole->id);

		$user->sendEmailVerificationNotification();

		return $user;
	}
}
