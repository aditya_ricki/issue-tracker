<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
	'middleware' => 'auth:api',
], function () {
	Route::get('/me', [App\Http\Controllers\Api\UserController::class, 'me'])->name('api.me')->middleware('can:view-developer-dashboard');
	Route::post('/update-details', [App\Http\Controllers\Api\UserController::class, 'updateDetails'])->name('api.update-details');
	Route::post('/update-password', [App\Http\Controllers\Api\UserController::class, 'updatePassword'])->name('api.update-password');
});

Route::post('/login', [App\Http\Controllers\Api\AuthController::class, 'login'])->name('api.login');
Route::post('/register', [App\Http\Controllers\Api\AuthController::class, 'register'])->name('api.register');
Route::get('/email-verification', [App\Http\Controllers\Api\VerificationController::class, 'verify'])->name('verification.verify');

Route::get('/authorize/{provider}/redirect', [App\Http\Controllers\Api\SocialAuthController::class, 'redirectToProvider'])->name('api.social.redirect');
Route::get('/authorize/{provider}/callback', [App\Http\Controllers\Api\SocialAuthController::class, 'handleProviderCallback'])->name('api.social.callback');

Route::post('/forgot-password', [App\Http\Controllers\Api\ForgotPasswordController::class, 'sendResetLinkEmail'])->name('api.forgot-password');
Route::post('/reset-password', [App\Http\Controllers\Api\ResetPasswordController::class, 'reset'])->name('api.reset-password');

ROute::get('/constans', [App\Http\Controllers\Api\ConstansController::class, 'index'])->name('api.constans');